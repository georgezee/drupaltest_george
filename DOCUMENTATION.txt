Kaplan Drupal Test Notes:

Deployment Instructions:
  1. Fetch the latest code from the Git repository (https://georgezee@bitbucket.org/georgezee/drupaltest_george.git)
  2. Ensure the root of the website is set to the /src directory.
  3. Create a /sites/default/files directory and make it writeable.
  4. Copy the /src/sites/default/default.settings.php to settings.php in the same folder.
  5. Run http://yoursite.com/install.php to configure the database settings, (or fill them in directly in settings.php)
  6. Import the database dump file provided to the database allocated to the site.
    - ** Note: This will replace the data in the target database, be sure to make backups first if necessary **
    - Use the file located at 'kaplan-test/kaplan-test-dump.sql'
    - e.g. mysql -u YOUR-USER -p YOUR-DATABASE < /YOUR-PATH-TO/kaplan-test/kaplan-test-dump.sql
  7. Visit the site to verify the new data has been loaded.
  8. The Admin account with the above dump is:
    u: admin
    p: hut4mill#


Notes:
  1. Though the instructions did specify the more custom modules the better, I decided against creating a custom module to define the Kaplan content type.
  2. There is a newer release of Drupal and Ctools with security fixes (as of 19 August), I thought it not necessary to install in this test environment. 
  3. I did intend to use the Features module for deployment, along with UUID_Features to retain the content. However I ran into the following issues which lead me to recommend the database import approach instead.
   - The content did not import reliably (sometimes the body and message field data were missing), though it would work if the import were repeated
   - The 'kaplan-test' URL was not retained, which would've required manual steps to set this up again.
   These issues could likely be resolved with additional investigation, but I believe it to be outside of the scope of this exercise. I did commit the feature created to an earlier commit for examination (usually I would create a separate branch for this).
  4. If I had to do this again, I would create another custom module using 'node_save' to create the 3 content items required programmatically. 
  5. 'drush make' would've been a good candidate for the deployment (perhaps with the backup and migrate module for the content), but I decided the database import was the simplest considering the circumstances.
  6. As the focus of the exercise doesn't seem to be on front end development, I made no theme or style changes.

I'd be happy to answer any questions you may have regarding the choices made for this exercise.

Thanks,
George Ziady