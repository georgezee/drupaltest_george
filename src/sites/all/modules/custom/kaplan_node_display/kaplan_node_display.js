jQuery(document).ready(function ($  ) {
    // This will automatically grab the 'title' attribute and replace
    // the regular browser tooltips for all <div> elements with a title attribute!
    $('div[title]').qtip();

});