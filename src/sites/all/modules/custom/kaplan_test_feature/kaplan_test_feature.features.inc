<?php
/**
 * @file
 * kaplan_test_feature.features.inc
 */

/**
 * Implements hook_node_info().
 */
function kaplan_test_feature_node_info() {
  $items = array(
    'kaplan' => array(
      'name' => t('kaplan'),
      'base' => 'node_content',
      'description' => t('Displays custom Kaplan content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
